//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/

	{"", "~/scripts/statusbar/disk",		60,		6},
	{"", "~/scripts/statusbar/temp",		60,		5},
	{"", "~/scripts/statusbar/volume",		0,		10},
	{"", "~/scripts/statusbar/networktraffic",	1,		4},
	{"", "~/scripts/statusbar/battery",		60,		2},
	{"", "date '+%d.%m.%Y (%a) %H:%M'",		5,		1},




};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;
